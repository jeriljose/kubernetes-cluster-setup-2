1. Go to the kubespray folder and

```
- sudo pip install -r requirements.txt
- cp -rfp inventory/sample inventory/mycluster
- declare -a IPS=(116.203.209.93 94.130.183.74 94.130.178.233)
- CONFIG_FILE=inventory/mycluster/hosts.yml python3 contrib/inventory_builder/inventory.py ${IPS[@]}
- ansible-playbook -i inventory/mycluster/hosts.yml --become --become-user=root cluster.yml
```
----------------------------------------------------------------------------------------------

# In the control machine

Copy /etc/kubernetes/admin.conf from the master or node1 and save it in /home/vargant/.kube folder

You cannot copy it directly to vagrant machine, so make a copy of it in the node1 or master server and then copy it to the vagrant machine as shown below

```
ssh vagrant@node1 sudo cp /etc/kubernetes/admin.conf /home/vagrant/config   # Make a copy of the config file
ssh vagrant@node1 sudo chmod +r ~/config                                   
mkdir .kube
scp vagrant@node1:~/config .kube/     # copy the config file to the .kube folder in the vagrant machine
```