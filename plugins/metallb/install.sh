#!/bin/bash

echo "Set up MetalLB"
kubectl apply -f metallb.yaml

echo "Set up MetalLB config file"
kubectl apply -f config.yaml
