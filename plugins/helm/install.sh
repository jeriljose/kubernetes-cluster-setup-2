#!/bin/bash

# DOWNLOAD AND INSTALL HELM IN YOUR VAGRANT BOX

wget https://get.helm.sh/helm-v2.14.3-linux-amd64.tar.gz
tar -xvzf helm-v2.14.3-linux-amd64.tar.gz
sudo mv linux-amd64/helm /usr/local/bin
rm helm-v2.14.3-linux-amd64.tar.gz
rm -rf ./linux-amd64/

# INITIALIZE TILLER 

helm init

# UPDATE HELM REPO

helm repo update

# ENABLE RBAC FOR HELM

kubectl create serviceaccount --namespace kube-system tiller

kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller

kubectl patch deploy --namespace kube-system tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}' 

# SAMPLE SYNTAX TO INSTALL MYSQL

   ## helm install stable/mysql --name my-special-installation --set mysqlPassword=johnjose
