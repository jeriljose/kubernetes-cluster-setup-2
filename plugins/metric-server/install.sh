#!/bin/bash

echo "Set up the Metrics Server..."
kubectl create -f aggregated-metrics-reader.yaml

echo "Set up the Metrics Server..."
kubectl create -f auth-delegator.yaml

echo "Set up the Metrics Server..."
kubectl create -f auth-reader.yaml

echo "Set up the Metrics Server..."
kubectl create -f metrics-apiservice.yaml

echo "Set up the Metrics Server..."
kubectl create -f metrics-server-deployment.yaml

echo "Set up the Metrics Server..."
kubectl create -f metrics-server-service.yaml

echo "Set up the Metrics Server..."
kubectl create -f resource-reader.yaml
