#!/bin/bash

echo "Create the rook operator..."
kubectl create -f rook-operator.yaml

echo "Wait for all pods of rook-operator to be ready..."
kubectl wait --for=condition=Ready --all pod -n rook-system

echo "Create the rook cluster..."
kubectl create -f rook-cluster.yaml

echo "Wait for all pods under rook cluster to be ready..."
kubectl wait --for=condition=Ready --all pod -n rook

echo "Create the rook storage class..."
kubectl create -f rook-storageclass.yaml

echo "Wait for all pods of rook storage class to be ready..."
kubectl wait --for=condition=Ready --all pod -n rook

echo "Create the rook tools..."
kubectl create -f rook-tools.yaml
