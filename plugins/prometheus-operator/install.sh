# To install prometheus

# Prometheus is for the backend and grafana is for the front-end. Prometheus collects various metrics and grafana displays it as a dashboard

helm install --name monitoring --namespace monitoring stable/prometheus-operator

# Note: To display all the workloads of prometheus

  # kubectl get all -n monitoring


# Prometheus also has a dashboard GUI. To view the prometheus gui. However this is not required and you can use Grafana

  # kubectl edit service/monitoring-prometheus-oper-prometheus -n monitoring``` and change from CluserIP to LoadBalancer


# Grafana

  # kubectl get all -n monitoring

  # kubectl edit service/monitoring-grafana -n monitoring and change from CluserIP to LoadBalancer
