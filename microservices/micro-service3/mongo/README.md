Mongodb replica set instruction

https://maruftuhin.com/blog/mongodb-replica-set-on-kubernetes/

```

## To join the mongodb replicaset

Note: microservice1 is the name of the namespace created
      mongod-0 - name of the pod
      mongodb-service - name of the service
 
rs.initiate({_id: "MainRepSet", version: 1, members: [
       { _id: 0, host : "mongod-0.mongodb-service.microservice1.svc.cluster.local:27017" },
       { _id: 1, host : "mongod-1.mongodb-service.microservice1.svc.cluster.local:27017" },
       { _id: 2, host : "mongod-2.mongodb-service.microservice1.svc.cluster.local:27017" }
 ]});

```

## To create a new user

```
 db.getSiblingDB("admin").createUser({
      user : "jeriljose",
      pwd  : "johnjose",
      roles: [ { role: "root", db: "admin" } ]
 });

```

## To install mongodb client

https://fabianlee.org/2018/05/20/mongodb-installing-a-mongodb-client-on-ubuntu/

## connection string documentation - https://github.com/helm/charts/issues/1569

### Examples:

 "mongodb://mongod-0.mongodb-service,mongod-1.mongodb-service,mongod-2.mongodb-service:27017/dbname_?"

 mongodb://{IP-OF-THE-KUBE-NODE-1}:32151,{IP-OF-THE-KUBE-NODE-2}:30616,{IP-OF-THE-KUBE-NODE-3}:30167/ 
