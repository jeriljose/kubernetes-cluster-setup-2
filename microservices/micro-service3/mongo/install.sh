#!/bin/bash

echo "Exec to the first mongodb pod"
kubectl exec -it mongod-0 -c mongod-container bash -n ms3

echo "Initiate the mongo replica set"
mongo --eval 'rs.initiate({_id: "MainRepSet", version: 1, members: [
       { _id: 0, host : "mongod-0.mongodb-service.ms3.svc.cluster.local:27017" },
       { _id: 1, host : "mongod-1.mongodb-service.ms3.svc.cluster.local:27017" },
       { _id: 2, host : "mongod-2.mongodb-service.ms3.svc.cluster.local:27017" }
]});'


echo "Create a new user for mongodb"
mongo --eval 'db.getSiblingDB("admin").createUser({
      user : "jeriljose",
      pwd  : "johnjose",
      roles: [ { role: "root", db: "admin" } ]
});'

echo "Exit out of the pod"
exit

#--------------------------------------------------------------------

echo "Exec into the second mongo pod"
kubectl exec -it mongod-1 -c mongod-container bash -n ms3

echo "Login into the mongodb"
mongo --eval 'db.getSiblingDB('admin').auth("jeriljose", "johnjose");'
mongo --eval 'db.getMongo().setSlaveOk()'

echo "Exit out of the pod"
exit
