1. Do a docker build and tag it ```docker build -t your_dockerhub_username/node-demo ```
   ```docker images``` will show you the list of images created

2. Push it to docker registery
3. Add the container URL in kubernetes deployment for stateful set file
4. Run ```kubectl create -f <kubernetes deployment yaml>```


5. CI/CD with Google Skaffold  - skaffold will push the docker container to dockerhub

https://skaffold.dev/docs/getting-started/

syntax: skaffold dev --default-repo <myrepo>
command: skaffold dev --default-repo jerilcj3






