    # Common Installations
     sudo locale-gen en_IN
     sudo apt-get update
     sudo apt-get install software-properties-common -y
     sudo apt-get install unzip -y 
           
    # Install docker
     curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
     sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
     sudo apt-get update
     sudo apt-get install -y docker-ce 
     sudo usermod -aG docker ${USER}

    # Install docker-compose
     sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
     sudo chmod +x /usr/local/bin/docker-compose    

    # Download and setup nodejs
    # curl -sL https://deb.nodesource.com/setup_8.x -o nodesource_setup.sh
    # sudo bash nodesource_setup.sh
    # sudo apt-get install nodejs -y
    # sudo apt-get install build-essential -y

    # Install serverless framework for kubeless
    # sudo npm install -g serverless

    # Install Kubeless CLI to access serverless/kubeless functions
    # wget https://github.com/kubeless/kubeless/releases/download/v1.0.2/kubeless_linux-amd64.zip
    # unzip kubeless_linux-amd64.zip
    # sudo mv bundles/kubeless_linux-amd64/kubeless /usr/local/bin
    # rm -r bundles/
    # rm kubeless_linux-amd64.zip

    # Install Prisma CLI for prisma server
    sudo npm install -g prisma
  
    # Download and setup postgresql
    # sudo apt-get update
    # sudo apt-get install postgresql postgresql-contrib -y
   
    # Download and install MongoDB
     #sudo apt-get install mongodb -y 
     #sudo apt-get update
     #sudo service mongodb start

    # Download and install terraform
    # wget https://releases.hashicorp.com/terraform/0.12.6/terraform_0.12.6_linux_amd64.zip
    # unzip terraform_0.12.6_linux_amd64.zip
    # mv terraform /usr/local/bin    
  
    # Download and install kubectl
    curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
    chmod +x ./kubectl
    sudo mv ./kubectl /usr/local/bin/kubectl

    # Install ansible
    # sudo apt-get install python-pip python-dev -y
    # sudo -H pip install ansible==2.8.3

    # Install skaffold
    curl -Lo skaffold https://storage.googleapis.com/skaffold/releases/latest/skaffold-linux-amd64
    chmod +x skaffold
    sudo mv skaffold /usr/local/bin

    #---------CONTROL MACHINE SETUP START--------------------------------

    #        1. Create a control machine. SSH-KEY of the control machine should be set to LAPTOP KEY
    #        2. Create a new user named ```vagrant``` in the control machine and
    #             add the user to sudo group as ```usermod -aG sudo vagrant```

    #        3. Login as vagrant

    #           ```su - vagrant```

    #        4. Generate ```ssh-keygen```
    #        5. Now copy this ssh-key to the hetzner cloud.
    #        6. Now create node1, node2, node3 and configure LAPTOP SSH-KEY AND CONTROL MACHINES VAGRANT USER KEY.
    #        7. In /etc/hosts of the control machine-> copy the ip of node1, node2, node3

    #        8. git clone the repo
    
    #-----------------------CONTROL MACHINE SETUP END---------------------

    # Install python3 for kubespray
    sudo apt install python3-pip -y
    sudo pip3 install --upgrade pip
    sudo apt-get install python3-setuptools -y

    # Install cryptography for kubespray
    sudo pip install cryptography==2.7

    # Install ansible
    sudo -H pip install ansible==2.7.12
