    cd ~
   
    # Common Installations
    sudo locale-gen en_IN
    sudo apt-get update
    sudo apt-get install software-properties-common -y
    sudo apt-get install unzip -y        

    # Download and setup nodejs
    curl -sL https://deb.nodesource.com/setup_8.x -o nodesource_setup.sh
    sudo bash nodesource_setup.sh
    sudo apt-get install nodejs -y
    sudo apt-get install build-essential -y

    # Install serverless framework for kubeless
    sudo npm install -g serverless

    # Install Kubeless CLI to access serverless/kubeless functions
    wget https://github.com/kubeless/kubeless/releases/download/v1.0.2/kubeless_linux-amd64.zip
    unzip kubeless_linux-amd64.zip
    sudo mv bundles/kubeless_linux-amd64/kubeless /usr/local/bin
    rm -r bundles/
    rm kubeless_linux-amd64.zip

    # Install Prisma CLI for prisma server
    sudo npm install -g prisma   
  
    # Download and install kubectl
    curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
    chmod +x ./kubectl
    sudo mv ./kubectl /usr/local/bin/kubectl    

    # Install skaffold
    curl -Lo skaffold https://storage.googleapis.com/skaffold/releases/latest/skaffold-linux-amd64
    chmod +x skaffold
    sudo mv skaffold /usr/local/bin